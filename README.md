# e04-ne1600-001

IOC settings and startup command to instantiate a New Era syringe pumps NE-1600 IOC; using 'ne1x00' EPICS driver; controlled device is installed in E04 laboratory;