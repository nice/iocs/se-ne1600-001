# ---
require essioc
require ne1x00

epicsEnvSet("PORTNAME", "$(IOCNAME)")
epicsEnvSet("IPADDR", "172.30.32.17")
epicsEnvSet("IPPORT", "4002")
epicsEnvSet("LOCATION", "SE: $(IPADDR):$(IPPORT)")
epicsEnvSet("PREFIX", "SE-SEE:SE-NE1600-001")
epicsEnvSet("SCAN", "1")
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(ne1x00_DIR)db")

iocshLoad("$(essioc_DIR)/common_config.iocsh")
iocshLoad("$(ne1x00_DIR)/ne1600.iocsh")
# ...
